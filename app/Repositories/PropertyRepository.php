<?php

namespace App\Repositories;

use App\Address;
use Illuminate\Database\Eloquent\Model;

class PropertyRepository implements RepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        $record = $this->find($id);
        return $record->update($data);
    }

    public function delete($id)
    {
        $record = $this->find($id);
        if($this->model->destroy($id))
            return Address::destroy($record->address_id);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }
}
