<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'property';
    public $fillable = [
        'address_id',
    ];

    public function owners()
    {
        return $this->belongsToMany('App\User','owner');
    }

    public function address()
    {
        return $this->hasOne('App\Address','id', 'address_id');
    }
}
