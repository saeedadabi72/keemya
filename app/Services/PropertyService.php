<?php

namespace App\Services;

use App\Property;
use App\Repositories\PropertyRepository;
use App\User;

class PropertyService
{
    public $propertyRepository;

    public function __construct()
    {
        $this->propertyRepository = new PropertyRepository(new Property());
    }

    public function create(array $data)
    {
        $property = $this->propertyRepository->create($data);

        // attach owners to property
        foreach ($data['owner_id'] as $owner)
        {
            // if $owner and main_owner value matched then set main to 1 else 0
            $this->propertyRepository->find($property->id)->owners()->save(
                User::find($owner),
                ['main' => $owner == $data['main_owner'] ? 1 : 0]
            );
        }
    }

    public function update($id, array $data)
    {
        $this->propertyRepository->update($id, $data);

        // update owners for property
        $this->propertyRepository->find($id)->owners()->sync($data['owner_id']);
        foreach ($data['owner_id'] as $owner)
        {
            if($owner == $data['main_owner'])
            {
                $this->propertyRepository->find($id)->owners()->updateExistingPivot($owner, ['main' => 1]);
            }
        }
    }

    public function delete($id)
    {
        return $this->propertyRepository->delete($id);
    }

    public function items()
    {
        $properties = $this->propertyRepository->all();

        foreach ($properties as $property)
        {
            $properties['address'] = $property->address;
            $properties['owners'] = $property->owners;
        }

        return $properties;
    }

    public function item($id)
    {
        $property = $this->propertyRepository->find($id);
        $property['owners'] = $property->owners;
        $property['address'] = $property->address;

        return $property;
    }
}
