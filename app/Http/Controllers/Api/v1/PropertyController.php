<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\PropertyService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{
    public $propertyService;

    public function __construct(PropertyService $propertyService)
    {
        $this->propertyService = $propertyService;
    }

    public function items()
    {
        $properties = $this->propertyService->items();

        return $this->json($properties);
    }

    public function item($id)
    {
        $rules = ['id' => 'required|integer|exists:property,id'];

        if($validate = $this->validateJson(['id' => $id],$rules)) {
            return $validate;
        }

        $property = $this->propertyService->item($id);

        return $this->json($property);
    }

    public function create(Request $request)
    {
        $rules = [
            'address_id' => 'required|integer|exists:address,id|unique:property,address_id',
            'owner_id' => 'required|array|exists:users,id',
            'main_owner' => 'required|integer|exists:users,id',
        ];

        if($validate = $this->validateJson($request->all(),$rules)) {
            return $validate;
        }

        $this->propertyService->create($request->all());

        return $this->json([],'property created successfully');

    }

    public function update($id, Request $request)
    {
        $rules = [
            'address_id' => 'required|integer|exists:address,id',
            'owner_id' => 'required|array|exists:users,id',
            'main_owner' => 'required|integer|exists:users,id',
        ];

        if($validate = $this->validateJson($request->all(),$rules)) {
            return $validate;
        }

        $this->propertyService->update($id, $request->all());

        return $this->json([],'property updated successfully');
    }

    public function delete($id)
    {
        $rules = ['id' => 'required|integer|exists:property,id'];

        if($validate = $this->validateJson(['id' => $id],$rules)) {
            return $validate;
        }

        $property = $this->propertyService->delete($id);

        return $this->json([],'property deleted successfully');
    }
}
