<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('property', 'PropertyController@items');
Route::get('property/{id}', 'PropertyController@item');
Route::post('property', 'PropertyController@create');
Route::put('property/{id}', 'PropertyController@update');
Route::delete('property/{id}', 'PropertyController@delete');
